import numpy as np
from matrix_utils import split
from numba import jit

@jit
def strassen(A, B):
    """
	Computes matrix product by divide and conquer approach, recursively.
	Input: nxn matrices A and B
	Output: nxn matrix, product of A and B
	"""

    # Base case when size of matrices is 1x1
    if len(A) == 1:
        return A * B

    # Splitting the matrices into quadrants. This will be done recursively
    # until the base case is reached.
    A11, A12, A21, A22 = split(A)
    B11, B12, B21, B22 = split(B)

    # Computing the 7 products, recursively (p1, p2...p7)
    M1 = strassen(A11 + A22, B11 + B22)
    M2 = strassen(A21 + A22, B11)
    M3 = strassen(A11, B12 - B22)
    M4 = strassen(A22, B21 - B11)
    M5 = strassen(A11 + A12, B22)
    M6 = strassen(A21 - A11, B11 + B12)
    M7 = strassen(A12 - A22, B21 + B22)

    # Computing the values of the 4 quadrants of the final matrix c
    C11 = M1 + M4 - M5 + M7
    C12 = M3 + M5
    C21 = M2 + M4
    C22 = M1 - M2 + M3 + M6

    # Combining the 4 quadrants into a single matrix by stacking horizontally and vertically.
    C = np.vstack((np.hstack((C11, C12)), np.hstack((C21, C22))))
    return C

@jit
def bilinear_after_change_of_basis(A, B):
    """
	Computes matrix product by divide and conquer approach, recursively.
	Input: nxn matrices A and B
	Output: nxn matrix, product of A and B
	"""
    if len(A) == 1:
        return A * B

    # Splitting the matrices into quadrants. This will be done recursively
    # until the base case is reached.
    A11, A12, A21, A22 = split(A)
    B11, B12, B21, B22 = split(B)

    # Computing the 7 products recursively based on U_opt, V_opt
    M1 = bilinear_after_change_of_basis(A22, B22)
    M2 = bilinear_after_change_of_basis(A21, B21)
    M3 = bilinear_after_change_of_basis(A12, B12)
    M4 = bilinear_after_change_of_basis(A11, B11)
    M5 = bilinear_after_change_of_basis(A12 - A21, -B12 + B22)
    M6 = bilinear_after_change_of_basis(-A11 + A12, B12 - B21)
    M7 = bilinear_after_change_of_basis(-A12 + A22, -B11 + B12)

    # Computing the values of the 4 quadrants of the final matrix c based on W_opt_T
    C11 = M4 + M5
    C12 = M3 + M5 - M6 + M7
    C21 = M2 + M7
    C22 = M1 - M6

    # Combining the 4 quadrants into a single matrix by stacking horizontally and vertically.
    C = np.vstack((np.hstack((C11, C12)), np.hstack((C21, C22))))

    return C
