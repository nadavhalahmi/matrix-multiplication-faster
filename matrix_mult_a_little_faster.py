import numpy as np
from Strassen import split, bilinear_after_change_of_basis


def psi_func(M):
    M11, M12, M21, M22 = split(M)
    if len(M) == 1:
        return M
    M11 = psi_func(M11)
    M12 = psi_func(M12)
    M21 = psi_func(M21)
    M22 = psi_func(M22)
    return np.vstack((np.hstack((M11, M12 - M21 + M22)), np.hstack((-M21 + M22, M12 + M22))))


def psi_func_reversed(M):
    M11, M12, M21, M22 = split(M)
    if len(M) == 1:
        return M
    M11 = psi_func_reversed(M11)
    M12 = psi_func_reversed(M12)
    M21 = psi_func_reversed(M21)
    M22 = psi_func_reversed(M22)
    return np.vstack((np.hstack((M11, M12 - M21)), np.hstack((-M12 + M22, -M12 + M21 + M22))))


def matrix_multiplication_a_little_faster(A, B, phi=psi_func, psi=psi_func, nhi_reverse=psi_func_reversed,
                                          ALG=bilinear_after_change_of_basis):
    A_tilda = phi(A)
    B_tilda = psi(B)
    C_tilda = ALG(A_tilda, B_tilda)
    C = nhi_reverse(C_tilda)
    return C

