import random
import time

from numpy import allclose

from matrix_utils import get_matrix_from_path, gen_random_matrix, classic_mul
from Strassen import strassen
import argparse
import numpy as np
from matrix_mult_a_little_faster import matrix_multiplication_a_little_faster


def main():
    # parser = argparse.ArgumentParser()
    # parser.add_argument('--matrixA', default='matrices/matrixA.txt')
    # parser.add_argument('--matrixB', default='matrices/matrixB.txt')
    # args = parser.parse_args()
    # matrixA_path = args.matrixA
    # matrixB_path = args.matrixB
    #
    # matrix_A = get_matrix_from_path(matrixA_path)
    # matrix_B = get_matrix_from_path(matrixB_path)
    debug = False
    matrix_A = gen_random_matrix([1024, 1024], lambda: random.uniform(-1000, 1000))
    matrix_B = gen_random_matrix([1024, 1024], lambda: random.uniform(-1000, 1000))
    t0 = time.time()
    C_matmul = np.matmul(matrix_A, matrix_B)
    t1 = time.time()
    print("matmul time:", f"{t1-t0:.2f}")
    if debug:
        print("C =")
        print(C_matmul)
    t0 = time.time()
    C_classic = classic_mul(matrix_A, matrix_B)
    t1 = time.time()
    print("classic time:", f"{t1-t0:.2f}")
    if debug:
        print("C =")
        print(C_classic)
    t0 = time.time()
    C_strassen = strassen(matrix_A, matrix_B)
    t1 = time.time()
    print("strassen time:", f"{t1-t0:.2f}")
    if debug:
        print("C =")
        print(C_strassen)
    t0 = time.time()
    C_mm_faster = matrix_multiplication_a_little_faster(matrix_A, matrix_B)
    t1 = time.time()
    print("mm faster time:", f"{t1-t0:.2f}")
    if debug:
        print("C =")
        print(C_mm_faster)
    results = [C_matmul, C_classic, C_strassen, C_mm_faster]
    print(all([allclose(results[0], curr) for curr in results]))


if __name__ == '__main__':
    main()
