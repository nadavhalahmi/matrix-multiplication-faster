import numpy as np
from numba import jit

@jit
def split(matrix):
    """
	Splits a given matrix into quarters.
	Input: nxn matrix
	Output: tuple containing 4 n/2 A n/2 matrices corresponding to a, b, c, d
	"""
    row, col = matrix.shape
    row2, col2 = row // 2, col // 2
    return matrix[:row2, :col2], matrix[:row2, col2:], matrix[row2:, :col2], matrix[row2:, col2:]


def gen_matrix(inFile, mat_size):
    datalines = inFile
    newMatrix = []

    for x in range(0, mat_size[0]):
        dataline = datalines.readline().split()
        for y in range(0, mat_size[1]):
            yVals = list(map(float, dataline))

        newMatrix.append(yVals)
    return np.matrix(newMatrix)


def get_matrix_from_path(path_to_matrix):
    matrix_file = open(path_to_matrix, 'r+')
    mat_size = [int(n) for n in matrix_file.readline().split()]
    return gen_matrix(matrix_file, mat_size)


def gen_random_matrix(mat_size, dist):
    newMatrix = []
    curr_line = []
    for x in range(0, mat_size[0]):
        for y in range(0, mat_size[1]):
            curr_line.append(dist())
        newMatrix.append(curr_line)
        curr_line = []
    return np.matrix(newMatrix)


@jit
def classic_mul(A: np.matrix, B: np.matrix):
    C = np.zeros((A.shape[0], B.shape[1]))
    for i in range(A.shape[0]):
        for j in range(B.shape[1]):
            for k in range(A.shape[1]):
                C[i, j] += A[i, k] * B[k, j]
    return C
